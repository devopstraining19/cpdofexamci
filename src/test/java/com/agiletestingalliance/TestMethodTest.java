package com.agiletestingalliance;

import static org.junit.Assert.*;
import java.io.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class TestMethodTest {

    @Test
    public void testFunc() throws Exception {

        TestMethod testMeth = new TestMethod("testing");
        assertTrue("Verify ",testMeth.gstr().equals("testing"));
    }

}
