package com.agiletestingalliance;

import static org.junit.Assert.*;
import java.io.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class DurationTest {

    @Test
    public void testFunc() throws Exception {

        Duration duration = new Duration();
	duration.calculateIntValue();
        assertTrue("Verify dur()",duration.dur().contains("CP-DOF is designed specifically"));
    }

}

