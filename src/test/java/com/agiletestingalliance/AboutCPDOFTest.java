package com.agiletestingalliance;

import static org.junit.Assert.*;
import java.io.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class AboutCPDOFTest {

    @Test
    public void testFunc() throws Exception {

        AboutCPDOF about = new AboutCPDOF();
        assertTrue("Verify desc()",about.desc().contains("CP-DOF certification program"));
    }

}

