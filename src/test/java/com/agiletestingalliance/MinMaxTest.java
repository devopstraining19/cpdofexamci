package com.agiletestingalliance;

import static org.junit.Assert.*;
import java.io.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class MinMaxTest {

    @Test
    public void testFunc() throws Exception {

        MinMax minMax = new MinMax();
	assertEquals("Verify returnBigger",10,minMax.returnBigger(5,10));
    }

    @Test
    public void testFunci2() throws Exception {

        MinMax minMax = new MinMax();
        assertEquals("Verify other","testing",minMax.bar("testing"));
    }

    @Test
    public void testFunc3() throws Exception {

        MinMax minMax = new MinMax();
        assertEquals("Verify returnBigger",10,minMax.returnBigger(10,5));
    }

    @Test
    public void testFunci4() throws Exception {

        MinMax minMax = new MinMax();
        assertEquals("Verify other",null,minMax.bar(null));
    }

    @Test
    public void testFunci5() throws Exception {

        MinMax minMax = new MinMax();
        assertEquals("Verify other","",minMax.bar(""));
    }


}
