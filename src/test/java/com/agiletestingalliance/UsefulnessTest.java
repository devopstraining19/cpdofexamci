package com.agiletestingalliance;

import static org.junit.Assert.*;
import java.io.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class UsefulnessTest {

    @Test
    public void testFunc() throws Exception {

        Usefulness usefulness = new Usefulness();
	usefulness.functionWF();
        assertTrue("Verify desc()",usefulness.desc().contains("DevOps is about transformation"));
    }

}
